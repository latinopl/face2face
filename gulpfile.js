process.env.DISABLE_NOTIFIER = true;
var gulp    = require('gulp');
var del     = require('del');
var include = require('require-dir');
var elixir  = require('laravel-elixir');

var $ = elixir.Plugins;


elixir.extend('bundle', function(src, output)
{
	var paths = new elixir.GulpPaths().src(src).output(output);

	new elixir.Task('bundle', function()
	{
		this.log(paths.src, paths.output);

		return (
			gulp
				.src(paths.src.path)
				.pipe($.if(! paths.output.isDir, $.rename(paths.output.name)))
				.pipe(gulp.dest(paths.output.baseDir))
		);
	})
	.watch(paths.src.path)
	.ignore(paths.output.path);
});

/* Wczytanie pliku gulpfile.js z katalogu szablonu */
include('./resources/assets');

