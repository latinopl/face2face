@extends('layouts.main')

@section('content')
    <input type='hidden' style='display:none;' ref="user_id" value="{{$user['id']}}" />
    @if(Auth::user()->id != $user['id'] && \App\Http\Controllers\UserProfileController::checkInvitation($user['id']) === 1)
    <input type='hidden' style='display:none;' ref="relation_settings" value="1" />
    @elseif(Auth::user()->id != $user['id'] && \App\Http\Controllers\UserProfileController::checkInvitation($user['id']) === 2)
    <input type='hidden' style='display:none;' ref="relation_settings" value="2" />
    @elseif(Auth::user()->id != $user['id'] && \App\Models\UserFriend::areWeFriends($user['id']) === false)
    <input type='hidden' style='display:none;' ref="relation_settings" value="3" />
    @elseif(Auth::user()->id != $user['id'] && \App\Models\UserFriend::areWeFriends($user['id']) === true)
    <input type='hidden' style='display:none;' ref="relation_settings" value="4" />
    @else
    <input type='hidden' style='display:none;' ref="relation_settings" value="0" />
    @endif

    <div class="row profile-header">
        <div class='col main-photo'>
            <img src="{{$user['photo']}}" />
        </div>
        <div class='col name'>
            <h2>{{$user['name']}}</h2>
            <span class="btn-friend btn btn-primary disabled" v-if="relation_settings == 1">
                {{ __('Wysłano zaproszenie') }}
            </span>
            <span @click='manageFriends("accept")' class="btn-friend btn btn-primary" v-if="relation_settings == 2">
                {{ __('Zaakceptuj zaproszenie') }}
            </span>
            <span @click='manageFriends("add")' class="btn-friend btn btn-primary" v-if="relation_settings == 3">
                {{ __('Dodaj do znajomych') }}
            </span>
            <span @click='manageFriends("remove")' class="btn-friend btn btn-primary" v-if="relation_settings == 4">
                {{ __('Usuń ze znajomych') }}
            </span>
        </div>
    </div>
    <div class='row user-menu'>
        <ul>
            <li v-bind:class="{active: tab === 0}" @click="tab = 0"><span>Posty</span></li>
            <li v-bind:class="{active: tab === 1}" @click="getUsersFriends()"><span>Znajomi</span></li>
        </ul>
    </div>
    <div v-if="tab === 0">
        @include('partials.postsSection')
    </div>
    <div v-if="tab === 1">
        @include('partials.friendsListSection')
    </div>
@endsection

@section('scriptsFooter')
    @include('partials.scripts.user_profile')
@endsection