@extends('layouts.main')

@section('content')
    @include('partials.postsSection')
@endsection

@section('scriptsFooter')
    @include('partials.scripts.home')
@endsection