<div class='mainTopBar fixed-right'>
    <div class='content'>
        <div class="logo">
            <a class="f2f-logo" href="{{route('home')}}" alt="Home"><span>{{__('Face2Face')}}</span></a>
        </div>
        <div class='userInterface'>
            @if(Auth::check())
                <div class="profileLink">
                    <a href='{{route('profile', ['id' => Auth::id()])}}' title='{{__('Profil użytkownika')}}'>
                        <span class='mainPhoto'>
                        @if(!empty(Auth::user()->main_photo))
                            <img src='{{url(Auth::user()->main_photo)}}'/>
                        @else
                            <img src='{{url('/images/male_placeholder.png')}}' />
                        @endif
                        </span>
                        <span>{{Auth::user()->first_name}}</span>
                    </a>
                </div>
                <div><a class="f2f-home" href='{{route('home')}}' title='{{__('Strona główna')}}'><span>{{__('Strona główna')}}</span></a></div>
                <div><a class="f2f-cog" href='{{route('settings')}}' title='{{__('Ustawienia konta')}}'><span>{{__('Ustawienia')}}</span></a></div>
                <div><a class="f2f-power-off" href='{{route('logout')}}' title='{{__('Wyloguj się z serwisu')}}'><span>{{__('Wyloguj się')}}</span></a></div>
            @endif
        </div>
        <div class='clearfix'></div>
    </div>
</div>

