<div class='notifications'>
    <div class="notification vue-notification" v-for="(notification, index) in notifications" v-bind:class='notification.type' @click.stop="removeNotification(index)">
        <div class="notification-title">@{{notification.title}}</div> 
        <div class="notification-content">@{{notification.content}}</div>
    </div>
</div>