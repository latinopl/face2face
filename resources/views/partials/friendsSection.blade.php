<h1>Lista znajomych</h1>
<div class='row' v-for='friend in friends'>
    <div class='col mainPhoto'>
        <a v-bind:href='friend.profile_link'><img v-bind:src='friend.image' class=''></a>
    </div>
    <div class='col name-col'>
        <a v-bind:href='friend.profile_link'>@{{friend.name}}</a>
    </div>
    <div class='col activity-col'>
        <span v-bind:class='friend.logged_icon' class="activity-icon"></span>
    </div>
</div>