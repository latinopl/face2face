@extends('layouts.main')

@section('content')
    {!! Form::standard($user)->url('/settings') !!}
        <div class="row">
            <div class="col">
                {!! Form::group()->text('first_name')->label('Imię')->required(true) !!}
            </div>
            <div class="col">
                {!! Form::group()->text('second_name')->label('Nazwisko')->required(true) !!}
            </div>
        </div>
        <div class="row">
            <div class="col">
                {!! Form::group()->select('sex', [0 => 'Nie podaję', 1 => 'Kobieta', 2 => 'Mężczyzna' ])->label('Płeć') !!}
            </div>
            <div class="col">
                {!! Form::group()->text('nick_name')->label('Przezwisko') !!}
            </div>
        </div>
        <div class="row">
            <div class="col">
                {!! Form::group()->textarea('about_me')->label('Opis') !!}
            </div>
        </div>
        {!! Form::group()
            ->add(Form::submit('register', 'Zapisz')->icon(Fluent::FA_DOWNLOAD)->css('btn-primary'))
        !!}
    {!! Form::close() !!}
@endsection

@section('scriptsFooter')
    @include('partials.scripts.settings')
@endsection