@extends('layouts.welcome')

@section('content')
@if(session()->has('auth'))
@php
    $errors = session('auth');
    dump($errors);
@endphp
<div><h1>{{ $errors['status'] }}</h1></div>
@endif
    <div class="row" id="authForms">
        <div v-if="action===0" class="col loginForm">
            <h2>{{ __('Masz konto? Zaloguj się!') }}</h2>
            {!! Form::horizontal()->errors($errors->getBag('default')) !!}
            {!! Form::group()->text('login_username')->label('Login lub e-mail') !!}
            {!! Form::group()->password('login_password')->label('Password') !!}
            <span v-on:click="action = 1" class='remindPasswordBtn'>{{ __('Przypomnij hasło') }}</span>
            {!! Form::group()
                ->add(Form::submit('login', 'Zaloguj się')->icon(Fluent::FA_DOWNLOAD)->css('btn-primary'))
            !!}
            {!! Form::close() !!}
        </div>
        <div v-if="action===1" class="col remindForm">
            <h2>{{ __('Wygeneruj nowe hasło!') }}</h2>
            {!! Form::horizontal()->url('/remind')->errors($errors->getBag('default')) !!}
            {!! Form::group()->email('remind_email')->label('Adres e-mail')->required(true) !!}
            <span v-on:click="action = 0" class='loginBtn'>{{ __('Zaloguj się') }}</span>
            {!! Form::group()
                ->add(Form::submit('remind', 'Przypomnij')->icon(Fluent::FA_DOWNLOAD)->css('btn-primary'))
            !!}
            {!! Form::close() !!}
        </div>
        <div class="col">
            <h2>{{ __('Nie masz konta? Zarejestruj się') }}</h2>
            {!! Form::standard()->url('/register')->errors($errors->getBag('App\Http\ViewModels\User\CreateViewModel')) !!}
            <div class="row">
                <div class="col">
                    {!! Form::group()->email('email')->label('Adres e-mail')->required(true) !!}
                </div>
                <div class="col">
                    {!! Form::group()->text('userlogin')->label('Login') !!}
                </div>
            </div>
            <div class="row">
                <div class="col">
                    {!! Form::group()->password('password')->label('Hasło')->required(true) !!}
                </div>
                <div class="col">
                    {!! Form::group()->password('password2')->label('Powtórz hasło')->required(true) !!}
                </div>
            </div>
            <div class="row">
                <div class="col">
                    {!! Form::group()->text('first_name')->label('Imię')->required(true) !!}
                </div>
                <div class="col">
                    {!! Form::group()->text('second_name')->label('Nazwisko')->required(true) !!}
                </div>
            </div>
            <div class="row">
                <div class="col">
                    {!! Form::group()->datetime('date_of_birth')->label('Data urodzenia')->required(true) !!}
                </div>
                <div class="col">
                    {!! Form::group()->select('sex', [0 => 'Nie podaję', 1 => 'Kobieta', 2 => 'Mężczyzna' ])->label('Płeć') !!}
                </div>
            </div>
            <div class="row">
                <div class="col">
                    {!! Form::group()->text('nick_name')->label('Przezwisko') !!}
                </div>
                <div class='col'></div>
            </div>
            {!! Form::group()
                ->add(Form::submit('register', 'Zarejestruj')->icon(Fluent::FA_DOWNLOAD)->css('btn-primary'))
            !!}
    {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scriptsFooter')
    @include('partials.scripts.auth')
@endsection