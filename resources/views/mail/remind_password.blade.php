@component('mail::message')
{{__('Witaj')}} {{$user->first_name}}!

{{__('Twoje hasło zostało zresetowane. Od teraz zalogowanie do konta będzie możliwe przy pomocy nowego hasła.')}}

{{__('Twoje nowe hasło to: ')}} {{$pw_new}}
@endcomponent