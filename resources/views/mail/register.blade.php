@component('mail::message')
{{__('Witaj')}} {{$user->first_name}}!

{{__('Dziękujemy bardzo za zarejestrowanie się w naszym serwisie. \n\
Drogi użytkowniku, dzieli Cię już tylko jeden krok od  korzystania z najlepszego serwisu społecznościowego. \n\
Wystarczy kliknąć w link aktywacyjny znajdującey się poniżej.')}}

@component('mail::button', ['url' => url('/activate/' . $user->activation_code . '/' . $user->id)])
{{__('Aktywuj konto')}}
@endcomponent

{{__('Jeżeli powyższy odnośnik nie działa, skopiuj poniższy adres i wklej go w oknie swojej przeglądarki internetowej.')}}

{{__('Adres www:')}} {{url('/activate/' . $user->activation_code . '/' . $user->id)}}

@endcomponent