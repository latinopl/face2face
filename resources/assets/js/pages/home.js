var VueResource = require('vue-resource');
var Vue = require('vue');
var Notifications = require('vue-notification');
 
Vue.use(Notifications);
Vue.use(VueResource);
window.addEventListener('load', function () {
    var vm = new Vue({
        el: '#content',
        data: {
            post: {
                message: '',
                visibility: 0
            },
            displayPostForm: true,
            comment: {
                message: ''
            },
            posts: [],
            limit: 20,
            offset: 0,
            notifications: [],
            showMorePosts: false
        },
        mounted: function(){
            this.getPosts(false);
        },
        methods: {
            setNotification: function(type, title, content)
            {
                let notification = {
                    type: type,
                    title: title,
                    content: content
                };
                let index = this.notifications.push(notification) - 1;
                setTimeout(() => this.removeNotification(index), 3000);
            },
            removeNotification: function(index)
            {
                this.notifications.splice(index, 1);
            },
            loadMorePosts: function(){
                this.offset += this.limit;
                this.getPosts(false);
            },
            getPosts: function(replace){
                let url = '/posts/0/' + this.limit + '/' + this.offset;
                this.$http.get(url).then(response => {
                    if(replace === true)
                    {
                        this.posts = [];
                    }
                    if(response.body.length >= this.limit)
                    {
                        this.showMorePosts = true;
                    }
                    else
                    {
                        this.showMorePosts = false;
                    }
                    this.posts = this.posts.concat(response.body);
                }, response => {
                    this.setNotification('error', 'Błąd', 'Błąd podczas ładowania postów');
                });
            },
            addComment: function(id, index)
            {
                let form = new FormData();
                form.append('message', this.posts[index].commentModel.message);
                form.append('post_id', id);
                this.$http.headers.common['Content-Type'] = 'multipart/form-data';
                this.$http.post('/comment', form).then(response => {
                    this.setNotification('success', 'Sukces', 'Komentarz został dodany');
                    this.getPosts(id, index, true);
                }, response => {
                    this.setNotification('error', 'Błąd', 'Komentarz nie został dodany');
                });
            },
            loadMoreComments: function(id, index)
            {
                this.posts[index].commentsOffset += this.posts[index].commentsLimit;
                this.getComments(id, index, false);
            },
            getComments: function(id, index, replace){
                let url = '/comments/' + id + '/' + this.posts[index].commentsLimit + '/' + this.posts[index].commentsOffset;
                this.$http.get(url).then(response => {
                    if(replace === true)
                    {
                        this.posts[index].comments = [];
                    }
                    if(response.body.length >= this.posts[index].commentsLimit)
                    {
                        this.posts[index].showMoreComments = true;
                    }
                    else
                    {
                        this.posts[index].showMoreComments = false;
                    }
                    this.posts[index].comments = this.posts[index].comments.concat(response.body);
                }, response => {
                    this.setNotification('error', 'Błąd', 'Wystąpił problem z załadowaniem komentarzy. Spróbuj ponownie poźniej');
                });
            },
            displayComments: function(id, index){
                if(this.posts[index].displayCommentsSection === true)
                {
                    this.posts[index].displayCommentsSection = false;
                }
                else
                {
                    this.posts[index].displayCommentsSection = true;
                    this.getComments(id, index, false);
                }
            },
            likePost: function(id, index)
            {
                this.$http.post('/post/like/'+id).then(response => {
                    let data = response.body;
                    if(data.status == '200')
                    {
                        if(data.action === 'unliked' && this.posts[index]['likes'] > 0)
                        {
                            this.posts[index]['likes'] -= 1;
                            this.posts[index]['likeIcon'] = 'f2f-thumbs-o-up';
                            this.setNotification('success', 'Sukces', 'Polubienie zostało usunięte');
                        }
                        else if(data.action === 'liked')
                        {
                            this.posts[index]['likes'] += 1;
                            this.posts[index]['likeIcon'] = 'f2f-thumbs-up';
                            this.setNotification('success', 'Sukces', 'Post został polubiony');
                        }
                    }
                });
            },
            publishPost: function()
            {
                let form = new FormData();
                form.append('message', this.post.message);
                form.append('visibility', this.post.visibility);
                this.$http.headers.common['Content-Type'] = 'multipart/form-data';
                this.$http.post('/post', form).then(response => {
                    this.limit = 20;
                    this.offset = 0;
                    this.getPosts(true);
                    this.setNotification('success', 'Sukces', 'Post został opublikowany');
                }, response => {
                    this.setNotification('error', 'Błąd', 'Wystąpił błąd podczas dodawania postu. Spróbuj ponownie');
                });
            },
        }
    })
})