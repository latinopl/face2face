var VueResource = require('vue-resource');
var Vue = require('vue');
var Notifications = require('vue-notification');
 
Vue.use(Notifications);
Vue.use(VueResource);
window.addEventListener('load', function () {
    var vm = new Vue({
        el: '#content',
        data: {
            comment: {
                message: ''
            },
            displayPostForm: false,
            posts: [],
            limit: 20,
            offset: 0,
            notifications: [],
            showMorePosts: false,
            userId: '',
            relation_settings: 0,
            tab: 0,
            friendsList: []
        },
        mounted: function(){
            this.getPosts(false);
            this.relation_settings = this.$refs.relation_settings.value;
        },
        methods: {
            setNotification: function(type, title, content)
            {
                let notification = {
                    type: type,
                    title: title,
                    content: content
                };
                let index = this.notifications.push(notification) - 1;
                setTimeout(() => this.removeNotification(index), 3000);
            },
            removeNotification: function(index)
            {
                this.notifications.splice(index, 1);
            },
            loadMorePosts: function(){
                this.offset += this.limit;
                this.getPosts(false);
            },
            showTab: function(int){
                if(this.tab == int)
                    return true;
                else
                    return false;
            },
            getPosts: function(replace){
                let user_id = this.$refs.user_id.value;
                let url = '/posts/'+user_id+'/' + this.limit + '/' + this.offset;
                this.$http.get(url).then(response => {
                    if(replace === true)
                    {
                        this.posts = [];
                    }
                    if(response.body.length >= this.limit)
                    {
                        this.showMorePosts = true;
                    }
                    else
                    {
                        this.showMorePosts = false;
                    }
                    this.posts = this.posts.concat(response.body);
                }, response => {
                    this.setNotification('error', 'Błąd', 'Błąd podczas ładowania postów');
                });
            },
            getUsersFriends: function()
            {
                this.tab = 1;
                let user_id = this.$refs.user_id.value;
                let url = '/relation/user/getUsersFriendsList/'+user_id;
                this.$http.get(url).then(response => {
                    if(response.body.status == "true")
                    {
                        this.friendsList = response.body.friends;
                    }
                    else
                    {
                        this.setNotification('error', 'Błąd', 'Błąd podczas ładowania listy znajomych');
                    }
                }, response => {
                    this.setNotification('error', 'Błąd', 'Błąd podczas ładowania listy znajomych');
                });
                console.log(this.friendsList);
            },
            manageFriends: function(action)
            {
                let user_id = this.$refs.user_id.value;
                this.$http.post('/relation/user/'+action+'/'+user_id).then(response => {
                    this.relation_settings = response.body.relation_settings;
                    this.setNotification(response.body.type, response.body.title, response.body.message);
                });
            },
            addComment: function(id, index)
            {
                let form = new FormData();
                form.append('message', this.posts[index].commentModel.message);
                form.append('post_id', id);
                this.$http.headers.common['Content-Type'] = 'multipart/form-data';
                this.$http.post('/comment', form).then(response => {
                    this.setNotification('success', 'Sukces', 'Komentarz został dodany');
                    this.getPosts(id, index, true);
                }, response => {
                    this.setNotification('error', 'Błąd', 'Komentarz nie został dodany');
                });
            },
            loadMoreComments: function(id, index)
            {
                this.posts[index].commentsOffset += this.posts[index].commentsLimit;
                this.getComments(id, index, false);
            },
            getComments: function(id, index, replace){
                let url = '/comments/' + id + '/' + this.posts[index].commentsLimit + '/' + this.posts[index].commentsOffset;
                this.$http.get(url).then(response => {
                    if(replace === true)
                    {
                        this.posts[index].comments = [];
                    }
                    if(response.body.length >= this.posts[index].commentsLimit)
                    {
                        this.posts[index].showMoreComments = true;
                    }
                    else
                    {
                        this.posts[index].showMoreComments = false;
                    }
                    this.posts[index].comments = this.posts[index].comments.concat(response.body);
                }, response => {
                    this.setNotification('error', 'Błąd', 'Wystąpił problem z załadowaniem komentarzy. Spróbuj ponownie poźniej');
                });
            },
            displayComments: function(id, index){
                if(this.posts[index].displayCommentsSection === true)
                {
                    this.posts[index].displayCommentsSection = false;
                }
                else
                {
                    this.posts[index].displayCommentsSection = true;
                    this.getComments(id, index, false);
                }
            },
            likePost: function(id, index)
            {
                this.$http.post('/post/like/'+id).then(response => {
                    let data = response.body;
                    if(data.status == '200')
                    {
                        if(data.action === 'unliked' && this.posts[index]['likes'] > 0)
                        {
                            this.posts[index]['likes'] -= 1;
                            this.posts[index]['likeIcon'] = 'f2f-thumbs-o-up';
                            this.setNotification('success', 'Sukces', 'Polubienie zostało usunięte');
                        }
                        else if(data.action === 'liked')
                        {
                            this.posts[index]['likes'] += 1;
                            this.posts[index]['likeIcon'] = 'f2f-thumbs-up';
                            this.setNotification('success', 'Sukces', 'Post został polubiony');
                        }
                    }
                });
            }
        }
    })
})