var VueResource = require('vue-resource');
var Vue = require('vue');
var Notifications = require('vue-notification');
 
Vue.use(Notifications);
Vue.use(VueResource);
window.addEventListener('load', function () {
    var vm = new Vue({
        el: '#friends',
        data: {
            friends: []
        },
        mounted: function(){
            this.getFriends();
        },
        methods: {
            getFriends: function(){
                this.$http.get('/relation/getList').then(response => {
                    if(response.body.status === 'true')
                    {
                        this.friends = response.body.friends;
                    }
                });
            }
        }
    })
})