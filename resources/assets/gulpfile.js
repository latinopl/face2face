process.env.DISABLE_NOTIFIER = true;
var elixir = require('laravel-elixir');
var imagemin = require('gulp-imagemin');
require('laravel-elixir-browserify-official');
require('laravel-elixir-vueify');
var assets = 'resources/assets/';
var pubdir = 'public/';
var bower = assets + 'bower_components/';

elixir.config.sourcemaps = !elixir.config.production;
elixir.config.assetsPath = '';
elixir.config.publicPath = pubdir;
elixir.config.css.sass.folder = '';
elixir.config.css.folder = '';
elixir.config.css.autoprefix.options.browsers = ['> 1%', 'Last 2 versions', 'IE 9'];

gulp.task('imagemin', function() {
	   gulp.src(assets+'images/*')
		.pipe(imagemin({
			progressive: true
		}))
		.pipe(gulp.dest(assets+'images/'));
});



elixir(function (mix)
{
    mix.browserify('./resources/assets/js/pages/home.js', './public/js/pages/home.js');
    mix.browserify('./resources/assets/js/pages/user_profile.js', './public/js/pages/user_profile.js');
    mix.browserify('./resources/assets/js/pages/settings.js', './public/js/pages/settings.js');
    mix.browserify('./resources/assets/js/pages/friends.js', './public/js/pages/friends.js');
    mix
        // SASS files
        .sass(assets + 'sass/app.scss', pubdir + 'bundle/css/app.css')
        .copy(assets + 'images', pubdir + 'images')
        .copy(assets + 'fonts', pubdir + 'fonts')
        .scripts([
            bower + 'jquery/dist/jquery.min.js',
            bower + 'vue/dist/vue.js',
            pubdir + 'bundle/js/components.js',
            assets + 'js/app.js',
        ],
        pubdir + 'js/app.js', './')

        // Images
        .copy(assets + 'images', pubdir + 'images')


    mix.styles(pubdir + 'bundle/css/*.css', pubdir + 'css');
});


