<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Brak danych w tabeli.',
    'throttle' => 'Zbyt duża liczba prób logowania. Poczekaj :seconds sekund i spróbuj ponownie.',

];
