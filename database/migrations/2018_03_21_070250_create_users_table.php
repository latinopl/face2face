<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 128)->nullable(false);
            $table->string('password', 255)->nullable(false);
            $table->string('userlogin', 20)->nullable(true);
            $table->string('first_name', 64)->nullable(false);
            $table->string('second_name', 64)->nullable(false);
            $table->string('nick_name', 64)->nullable(true);
            $table->string('activation_code', 64)->nullable(true);
            $table->text('password_history')->comment('Serialized array of passwords')->nullable(true);
            $table->char('is_profile_public', 1)->nullable(false)->default(0);
            $table->char('is_activated', 1)->nullable(false)->default(0);
            $table->char('is_logged', 1)->nullable(false)->default(0);
            $table->timestamp('last_signin')->nullable(false)->default(now()->toDateTimeString());
            $table->timestamp('signup_date')->nullable(false)->default(now()->toDateTimeString());
            $table->timestamp('last_password_change')->nullable(false)->default(now()->toDateTimeString());
            $table->timestamp('last_account_change')->nullable(false)->default(now()->toDateTimeString());
            $table->text('about_me')->nullable(true);
            $table->date('date_of_birth')->nullable(false);
            $table->char('sex', 1)->comment('0 - empty, 1 - female, 2 - male')->nullable(false)->default(2);
            $table->string('main_photo', 255)->nullable(true);
            $table->string('remember_token', 255)->nullable(true);
            $table->timestamp('last_logout')->nullable(false)->default(now()->toDateTimeString());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
