<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    public function getIndex()
    {
        \App\Models\User::logUserActivity('SettingsController::getIndex');
        return view('settings.index')->with('user', Auth::user());
    }
    
    public function saveSettings()
    {
        $date = \Input::all();
        
        $user = Auth::user();
        $user->first_name = $date['first_name'];
        $user->second_name = $date['second_name'];
        $user->sex = $date['sex'];
        $user->nick_name = $date['nick_name'];
        $user->about_me = $date['about_me'];
        $user->last_account_change = \Carbon\Carbon::now();
        $user->save();
        
        return redirect(route('settings'));
    }
}
