<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserFriend;
use App\Models\UserFriendInvitation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserProfileController extends Controller
{
    public function getIndex($user_id)
    {
        \App\Models\User::logUserActivity('UserProfileController::getIndex');
        $user = User::query()->where('id', $user_id)->first();
        $data = [
            'id' => $user->id,
            'name' => $user->first_name . ' ' . $user->second_name,
            'photo' => User::getMainPhotoUrl($user)
        ];
        return view('user_profile.index')->with('user', $data);
    }
    
    public function getUsersFriendsList($user_id)
    {
        \App\Models\User::logUserActivity('UserProfileController::getUsersFriendsList');
        return response(json_encode(['status' => 'true', 'friends' => $this->getFriends($user_id)]), 200);
    }
    
    private function getFriends($userid)
    {
        $friends = [];
        
        $data = DB::select("SELECT u.id, u.first_name, u.second_name, u.main_photo, u.last_signin, u.last_logout, u.sex, max(ua.created_at) as last_activity FROM users as u LEFT JOIN user_friends as uf ON uf.friend_id = u.id LEFT JOIN user_activity as ua ON ua.user_id = u.id WHERE uf.user_id = " . $userid." GROUP BY u.id");
        
        foreach($data as $row)
        {
            $icon = 'offline-icon';
            
            if(User::isUserActive($row))
            {
                $icon = 'online-icon';
            }
            
            $friends[] = [
                'name' => $row->first_name . ' ' . $row->second_name,
                'profile_link' => route('profile', ['id' => $row->id]),
                'image' => User::getMainPhotoUrl($row),
                'logged_icon' => $icon
            ];
        }
        
        return $friends;
    }
    
    public function getFriendList()
    {
        \App\Models\User::logUserActivity('UserProfileController::getFriendList');

        return response(json_encode(['status' => 'true', 'friends' => $this->getFriends(Auth::user()->id)]), 200);
    }
    
    /**
     * Check if logged user invited another user 
     * 
     * @param type $user_id
     * @return int 0 - no invitation, 1 - logged user invited another user, 2 - another user invited logged user
     */
    public static function checkInvitation($user_id)
    {
        $check = UserFriendInvitation::query()->where('user_id', Auth::user()->id)->where('invited_user_id', $user_id)->count();
        if($check)
        {
            return 1;
        }
        else
        {
            $check = UserFriendInvitation::query()->where('invited_user_id', Auth::user()->id)->where('user_id', $user_id)->count();
            if($check)
                return 2;
        }
        
        return 0;
    }
    
    private function addFriend($user_id)
    {
        if(UserFriend::areWeFriends($user_id))
            return response(json_encode(['type' => 'alert', 'title' => 'Uwaga!', 'message' => 'Już jesteście znajomymi', 'relation_settings' => 3]), 200);
        
        if(self::checkInvitation($user_id))
            return response(json_encode(['type' => 'alert', 'title' => 'Uwaga!', 'message' => 'Zaproszenie oczekuje na zaakcpetowanie', 'relation_settings' => 3]), 200);
        
        $userFriendInv = new UserFriendInvitation();
        $userFriendInv->user_id = Auth::user()->id;
        $userFriendInv->invited_user_id = $user_id;
        $userFriendInv->created_at = \Carbon\Carbon::now();
        $userFriendInv->updated_at = \Carbon\Carbon::now();
        
        try
        {
            if($userFriendInv->save())
            {
                return response(json_encode(['type' => 'success', 'title' => 'Sukces', 'message' => 'Zaproszenie zostało wysłane', 'relation_settings' => 1]), 200);
            }
        }
        catch(Exception $e)
        {
            \App\Extensions\ErrorLog\ErrorLog::writeLog($e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine());
        }
        
        return response('Error', 400);
    }
    
    private function removeFriend($user_id)
    {
        if(!UserFriend::areWeFriends($user_id))
            return response(json_encode(['type' => 'alert', 'title' => 'Uwaga!', 'message' => 'Nie możesz usunąć znajomości która nie istnieje!', 'relation_settings' => 4]), 200);
        else
        {
            $friendship = DB::delete("DELETE FROM user_friends WHERE (user_id = ".$user_id." AND friend_id = ".Auth::user()->id.") OR (user_id = ".Auth::user()->id." AND friend_id = ".$user_id.")");
            
            if($friendship)
            {
                return response(json_encode(['type' => 'success', 'title' => 'Sukces', 'message' => 'Znajomość została usunięta', 'relation_settings' => 3]), 200);
            }
        }
        
        return response(json_encode(['type' => 'error', 'title' => 'Błąd', 'message' => 'Błąd', 'relation_settings' => 4]), 200);
    }
    
    private function acceptFriendInvitation($user_id)
    {
        if(self::checkInvitation($user_id) === 2)
        {
            DB::delete("DELETE FROM user_friend_invitations WHERE user_id = ".$user_id." AND invited_user_id = " . Auth::user()->id);
            
            $realtions = DB::insert("INSERT INTO user_friends (user_id, friend_id, created_at, updated_at) VALUES (".$user_id.", ".Auth::user()->id.", '".\Carbon\Carbon::now()."', '".\Carbon\Carbon::now()."'), (".Auth::user()->id.", ".$user_id.", '".\Carbon\Carbon::now()."', '".\Carbon\Carbon::now()."')");
        
            if($realtions)
            {
                return response(json_encode(['type' => 'success', 'title' => 'Sukces', 'message' => 'Zaakceptowano zaproszenie', 'relation_settings' => 4]), 200);
            }
        }
        
        return response(json_encode(['type' => 'error', 'title' => 'Błąd', 'message' => 'Spróbuj później', 'relation_settings' => 2]), 200);
    }
    
    public function manageFriends($action, $user_id)
    {
        \App\Models\User::logUserActivity('UserProfileController::manageFriends::'.$action);
        if($action === 'add')
        {
            return $this->addFriend($user_id);
        }
        
        if($action === 'remove')
        {
            return $this->removeFriend($user_id);
        }
        
        if($action === 'accept')
        {
            return $this->acceptFriendInvitation($user_id);
        }
        
        return response(json_encode(['type' => 'error', 'title' => 'Błąds', 'message' => 'Taka operacja nie jest dozwolona']), 200);
    }
}
