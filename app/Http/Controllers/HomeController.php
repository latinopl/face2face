<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function getIndex()
    {
        \App\Models\User::logUserActivity('HomeController::getIndex');
        return view('home');
    }
}
