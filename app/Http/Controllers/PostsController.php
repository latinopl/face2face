<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post as Post;
use App\Models\Postlike;
use App\Models\Postcomment;
use App\Http\ViewModels\Post\CreateViewModel as PostCreateViewModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function createPost(){
        \App\Models\User::logUserActivity('PostsController::createPost');
        try
        {
            $data = \Input::all();
            $postViewModel = new PostCreateViewModel($data);
            $postViewModel->allow_comments = true;
            $postViewModel->user_id = Auth::user()->id;
            $postViewModel->is_removed = false;
            $postViewModel->created_at = \Carbon\Carbon::now();
            $postViewModel->created_at = \Carbon\Carbon::now();

            if($postViewModel->isValid())
            {
                $postModel = new Post();
                $postModel->fill($postViewModel->toArray());
                if($postModel->save())
                {
                    return response('Post został dodany pomyślnie', 200);
                }
                
                return response('Nie udało się dodać postu. Spróbuj ponownie', 250);
            }
            else {
                return response('Treść nie może być pusta', 250);
            }
        }
        catch(\Exception $e)
        {
            \App\Extensions\ErrorLog\ErrorLog::writeLog($e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine());
            return response('Nie udało się dodać postu. Spróbuj ponownie', 260);
        }
    }
    
    public function getPosts($user_id = 0, $limit = 20, $offset = 0)
    {
        \App\Models\User::logUserActivity('PostsController::getPosts');
        return response(json_encode(Post::getPostsForUser($user_id, $limit, $offset)));
    }
    
    public function likePost($id = 0)
    {
        \App\Models\User::logUserActivity('PostsController::likePost');
        if($id > 0)
        {
            $like = Postlike::query()->where('post_id', $id)->where('user_id', Auth::user()->id)->first();
            if(empty($like->post_id))
            {
                $likeN = new Postlike();
                if($likeN->fill(['post_id' => $id, 'user_id' => Auth::id()])->save())
                {
                    return response(json_encode(['message'=> 'Polubiono', 'action' => 'liked', 'status' => 200]), 200);
                }
                else
                {
                    return response(json_encode(['message'=>'Błąd', 'status' => 250]), 250);
                }
            }
            else
            {
                unset($like);
                if(DB::delete('DELETE FROM postlikes WHERE user_id = :user_id AND post_id = :post_id', ['post_id' => $id, 'user_id' => Auth::user()->id]))
                {
                    return response(json_encode(['message'=> 'Usunięto polubienie', 'action' => 'unliked', 'status' => 200]), 200);
                }
                else
                {
                    return response(json_encode(['message'=>'Błąd', 'status' => 260]), 260);
                }
            }
        }
    }
    
    public function addComment()
    {
        \App\Models\User::logUserActivity('PostsController::addComment');
        try
        {
            $data = \Input::all();
            $comment = new Postcomment();
            $comment->post_id = $data['post_id'];
            $comment->user_id = Auth::user()->id;
            $comment->message = $data['message'];
            $comment->visibility = 0;
            $comment->is_removed = 0;
            $comment->likes = 0;
            $comment->created_at = \Carbon\Carbon::now();
            $comment->created_at = \Carbon\Carbon::now();

            if($comment->save())
            {
                return response('Post został dodany pomyślnie', 200);
            }
            else {
                return response('Nie udało się dodać komentarza. Spróbuj ponownie', 250);
            }
        }
        catch(\Exception $e)
        {
            \App\Extensions\ErrorLog\ErrorLog::writeLog($e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine());
            return response('Nie udało się dodać komentarza. Spróbuj ponownie', 260);
        }
    }
    
    public function getCommentsForPost($post_id, $limit, $offset)
    {
        \App\Models\User::logUserActivity('PostsController::getCommentsForPost');
        $query = DB::table('postcomments')->select('postcomments.*', 'users.first_name', 'users.second_name', 'users.main_photo')->where('post_id', $post_id)->join('users', 'postcomments.user_id', '=', 'users.id', 'left')->limit($limit)->offset($offset)->get();
        $comments = [];
        if(count($query) > 0)
        {
            foreach($query as $comment)
            {
                $userImage = !empty($comment->main_photo) ? url($comment->main_photo) : url('/images/male_placeholder.png');
                $comments[] = [
                    'id' => $comment->id,
                    'message' => $comment->message,
                    'time' => date($comment->created_at),
                    'user' => [
                        'name' => $comment->first_name . ' ' . $comment->second_name,
                        'image' => $userImage
                    ]
                ];
            }
        }
        return response(json_encode($comments), 200);
    }
}
