<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Http\ViewModels\User\CreateViewModel;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Mail;
use App\Mail\MailController;

class AuthController extends Controller {
    private $loginValidationRules = [
        'login_username' => 'required',
        'login_password' => 'required|min:8|max:20'
    ];
    
    const UNIQUE_SALT_STRING_PREFIX = 'to_jest_moje_małe_krulestwo_';
    const UNIQUE_SALT_STRING_POSTFIX = '_specjalnie_przez_u';


    use AuthenticatesUsers;
    
    private function getGenerateActivationCode($userName)
    {
        $string = sha1(self::UNIQUE_SALT_STRING_PREFIX . $userName . self::UNIQUE_SALT_STRING_POSTFIX);
        return sha1(self::UNIQUE_SALT_STRING_PREFIX . $string . self::UNIQUE_SALT_STRING_POSTFIX);
    }
    
    /**
     * Logowanie do serwisu
     *
     * @return Response
     */
    public function postLogin(Request $request)
    {
        $data = $request->all();
        $request->validate($this->loginValidationRules);
        
        $user = User::orWhere('email', $data['login_username'])->orWhere('userlogin', $data['login_username'])->select('id')->first();
        
        if(!empty($user->id) && $user->id > 0)
        {
            $user = User::where('id', $user->id)->where('is_activated', '1')->select('id')->first();
            if(empty($user->id) || $user->id <= 0)
            {
                return redirect()->route('login')->withErrors(['Konto nie zostało jeszcze aktywowane!']);
            }
        }
        else
        {
            return redirect()->route('login')->withErrors(['Brak takiego użytkownika!']);
        }
        
        if (Auth::attempt(['email' => $data['login_username'], 'password' => $data['login_password'], 'is_activated' => "1"]) || Auth::attempt(['userlogin' => $data['login_username'], 'password' => $data['login_password'], 'is_activated' => "1"])) {        
            User::where('id', Auth::id())->update(['is_logged' => '1', 'last_signin' => now()->toDateTimeString()]);
            return redirect()->route('home');
        }
        else
        {
            return back()->withErrors([0 => 'Niepoprawne dane logowania']);
        }
    }
    
    public function getLogin()
    {
        return view('auth.index');
    }
    
    public function getLogout()
    {
        User::where('id', Auth::id())->update(['is_logged' => '0', 'last_logout' => now()->toDateTimeString()]);
        Auth::logout();
        return redirect()->route('login');
    }
    
    public function getRemind()
    {
        return view('auth.remind');
    }
    
    public function postRemind()
    {
        $input = \Input::getAll();
        $user = User::where('email', $input['email'])->first();
        $pw_history = unserialize($user->password_history);
        $pw_new = $this->generatePassword(13, true, $pw_history);
        $pw_history = array_slice(array_merge([$pw_new], $pw_history), 0, 6);
        
        try
        {
            $user->password_history = $pw_history;
            $user->last_password_change = now()->toDateTimeString();
            $user->password = $pw_new;
            Mail::to($user->email)->send(new MailController('mail.remind_password', ['pw_new' => $pw_new]));
            $user->save();
        }
        catch(\Exception $e)
        {
            \ErrorLog::writeLog($e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine());
        }
        
        return redirect()->route('login');
    }
    
    /**
     * Generates password and eventually checks password history
     * 
     * @param int $length
     * @param bool $checkHistory
     * @param array $history
     */
    private function generatePassword($length = 13, $checkHistory = false, $history = [])
    {
        $pw_new = \Hash::make(\StringGenerator::random_str($length));
        if($checkHistory && !empty($history) && in_array($pw_new, $history))
        {
            $pw_new = $this->generatePassword($length, $checkHistory, $history);
        }
        
        return $pw_new;
    }
    
    public function postRegister()
    {
        $model = new CreateViewModel(\Input::all());
        if($model->isValid())
        {
            $entity = new User();
            $entity->fill($model->toArray());
            $entity->password = \Hash::make($entity->password);
            $entity->activation_code = $this->getGenerateActivationCode($entity->email);
            $entity->password_history = serialize([$entity->password]);
            $entity->last_account_change = now()->toDateTimeString();
            $entity->last_logout = now()->toDateTimeString();
            $entity->last_password_change = now()->toDateTimeString();
            $entity->last_signin = now()->toDateTimeString();
            $entity->signup_date = now()->toDateTimeString();
            $entity->save();
            
            try
            {
                Mail::to($entity->email)->send(new MailController('mail.register', ['user' => $entity]));
            }
            catch(\Exception $e)
            {
                $entity->delete();
                return back()->withErrors([$e->getMessage(),$e->getCode(),$e->getFile(),$e->getLine()]);
            }
            return redirect('/login')->with('status', 'registred');
        }
        else
        {
            return redirect('/login')->withErrors($model->getErrors());
        }
    }
    
    public function getActivate($code, $id)
    {
        $user = User::where('id', $id)->first();
        
        if(!empty($user->email) && $user->is_activated === '1')
        {
            return redirect('/login')->withErrors(['To konto zostało już aktywowane!']);
        }
        
        if(!empty($user->email) && $this->getGenerateActivationCode($user->email) === $code)
        {
            $user->is_activated = '1';
            $user->save();
            
            Mail::to($user->email)->send(new MailController('mail.activated', ['user' => $user]));
            
            return redirect('/login')->with(['success' => ['Konto zostało aktywowane. Teraz można zalogować się do serwisu!']]);
        }
        
        return redirect()->route('login')->withErrors(['Kod aktywacyjny niepoprawny']);
    }
}
