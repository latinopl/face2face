<?php namespace App\Http\ViewModels\Postcomment;

use App\Extensions\ViewModels\ViewModel;
use App\Models\Postcomment;

/**
 * Class EditViewModel
 *
 * @package App\Http\ViewModels\Postcomment\
 */
class EditViewModel extends ViewModel
{
    public $updated_at;
    public $visibility;
    public $message;
    
    /**
     * @param Content|array $data
     */
    public function __construct($data)
    {
        $this->fill($data);
    }

    /**
     * @return array
     */
    public function getValidatorRules()
    {
        return [
            
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return parent::toArray();
    }
}