<?php namespace app\Extensions\Eloquent\Constants;

/**
 * Class Versionable
 *
 * @package app\Extensions\Eloquent\Constants
 */
class Versionable
{
    const STATUS_DRAFT = 0;
    const STATUS_FINISHED = 1;
    const STATUS_REJECTED = 2;
    const STATUS_ACCEPTED = 3;

    /**
     * @param bool|false $canEdit
     * @param bool|false $canAccept
     * @return array
     */
    public static function getOptions($canEdit = false, $canAccept = false)
    {
        $statuses = [];
        
        if ($canEdit)
        {
            $statuses += [
                Versionable::STATUS_DRAFT => __('Szkic'),
                Versionable::STATUS_FINISHED => __('Ukończony')
            ];
        }
        if ($canAccept)
        {
            $statuses += [
                Versionable::STATUS_REJECTED => __('Zwrócony'),
                Versionable::STATUS_ACCEPTED => __('Zaakceptowany')
            ];
        }
        
        return $statuses;
    }
}