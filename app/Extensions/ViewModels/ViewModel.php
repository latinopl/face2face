<?php namespace App\Extensions\ViewModels;

use App\Extensions\ViewModels\Interfaces\IValidatableViewModel;
use Illuminate\Support\MessageBag;
use Illuminate\Support\ViewErrorBag;
use Illuminate\Validation\Validator;
use inkvizytor\FluentForm\Base\IModel;

abstract class ViewModel extends BaseViewModel implements IValidatableViewModel, IModel
{
    /** @var Validator */
    private $validator = null;

    /** @var bool */
    private $valid = null;

    /**
     * @return bool
     */
    public function isValid()
    {
        if ($this->valid === null)
        {
            $this->valid = $this->getValidator()->passes();
            
            $errors = $this->getValidator()->errors();
            $viewbag = \Session::get('errors', new ViewErrorBag);
            $viewbag->put(get_class($this), $errors);
   
            \Session::flash('errors', $viewbag);
        }
        
        return $this->valid;
    }

    /**
     * Returns Laravel's validator rules array
     * @return array
     */
    public function getValidatorRules()
    {
        return [];
    }

    /**
     * Returns Laravel's validator custom messages array
     * @return array
     */
    public function getCustomMessages()
    {
        return [];
    }

    /**
     * Create Laravel's native validator
     * @return Validator
     */
    public function createValidator()
    {
        return \Validator::make($this->getRawValues(), $this->getValidatorRules(), $this->getCustomMessages());
    }
    
    /**
     * Returns Laravel's native validator
     * @return Validator
     */
    public function getValidator()
    {
        if ($this->validator === null)
        {
            $this->validator = $this->createValidator();
        }
        
        return $this->validator;
    }

    /**
     * Returns laravel validator instance
     * @return MessageBag
     */
    public function getErrors()
    {
        if ($this->valid !== null)
        {
            return $this->getValidator()->errors();
        }
        
        /** @var ViewErrorBag $viewbag */
        $viewbag = \Session::get('errors', new ViewErrorBag);
        
        return $viewbag->getBag(get_class($this));
    }

    /**
     * @param $name
     * @return FileUpload
     */
    public function getFile($name)
    {
        if (is_array($this->{$name}))
        {
            return new FileUpload($this->{$name});
        }
        
        return null;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return $this->getValidatorRules();
    }

    /**
     * @return \Illuminate\Support\MessageBag
     */
    public function errors()
    {
        return $this->getErrors();
    }
} 