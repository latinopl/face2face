<?php namespace App\Extensions\ViewModels\Interfaces;

interface IViewModel
{
    /**
     * Returns array of public attributes' names
     * @return array
     */
    public function getPublicAttributes();
    
    /**
     * Fills the model with passed attributes.
     * If the model does not contain and
     * attribute's key, it's value's gonna be omitted.
     *
     * @param array|mixed $attributes
     * @return IViewModel
     */
    public function fill($attributes = array());
    
    /**
     * @return array
     */
    public function toArray();
    
    /**
     * @param int $options
     * @return string
     */
    public function toJson($options = 0);
} 