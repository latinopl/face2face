<?php namespace App\Extensions\ViewModels\Interfaces;

use Illuminate\Support\MessageBag;
use Illuminate\Validation\Validator;

interface IValidatableViewModel
{
    /**
     * Validates model and returns it's state.
     * True if it's valid, false if it's not.
     *
     * @return bool
     */
    public function isValid();
    
    /**
     * Returns laravel validator instance
     * @return Validator
     */
    public function getValidator();

    /**
     * Returns laravel validation errors
     * @return MessageBag
     */
    public function getErrors();
} 