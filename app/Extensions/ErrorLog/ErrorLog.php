<?php namespace App\Extensions\ErrorLog;

class ErrorLog {
    
    /**
    * @access public
    * @param int $code
    * @param string $message
    * @param string $file
    * @param int $line
    * @static
    */
    public static function writeLog($code, $message, $file, $line)
    {
        $dir = 'Error/';
        if (!is_dir($dir))
        {
            mkdir($dir, 0755, true);
        }

        $errFile = $dir . 'error_'.date('Y_m').'.php';

        if (!file_exists($errFile))
        {
            $xmlString = "<?php \n die;\n".'//<?xml version="1.0" encoding="UTF-8"?'.'>
    <ErrorLog>
        <Error date="'.date('Y-m-d H:i:s').'">
            <Message>
            <![CDATA[
            '.$message.'
            ]]>
            </Message>
            <InnerExceptionType>'.self::humanErrorType($code).'</InnerExceptionType>
            <File>'.$file.'</File>
            <Line>'.$line.'</Line>
        </Error>
    </ErrorLog>';

            $fp = fopen($errFile, 'a');
            if ($fp)
            {
                fwrite($fp, $xmlString);
                fclose($fp);
                chmod($errFile, 0644);
            }
        }
        else
        {
            if (date('Y') != date('Y', filemtime($errFile)))
            {
                unlink($errFile);

                $xmlString = "<?php \n die;\n".'//<?xml version="1.0" encoding="UTF-8"?'.'>
                    </ErrorLog>';

                $fp = fopen($errFile, 'a');
                if ($fp)
                {
                    fwrite($fp, $xmlString);
                    fclose($fp);
                    chmod($errFile, 0644);
                }
            }

            $xmlString = '	<Error date="'.date('Y-m-d H:i:s').'">
            <Message>
            <![CDATA[
            '.$message.'
            ]]>
            </Message>
            <InnerExceptionType>'.self::humanErrorType($code).'</InnerExceptionType>
            <File>'.$file.'</File>
            <Line>'.$line.'</Line>
            <RequestUri><![CDATA[ '.$_SERVER['REQUEST_URI'].' ]]></RequestUri>
        </Error>
</ErrorLog>';

            $fp = fopen($errFile, 'r+');
            if ($fp)
            {
                fseek($fp, -11, SEEK_END);
                fwrite($fp, $xmlString);
                fclose($fp);
                chmod($errFile, 0644);
            }
        }
    }
    
    /**
    * @access private
    * @param int $code
    * @return string
    */
    public static function humanErrorType($code)
    {
        switch($code)
        {
            case E_ERROR: //1 
                return 'E_ERROR';
            case E_WARNING: //2
                return 'E_WARNING';
            case E_PARSE: //4
                return 'E_PARSE';
            case E_NOTICE: //8
                return 'E_NOTICE';
            case E_CORE_ERROR: //16
                return 'E_CORE_ERROR';
            case E_CORE_WARNING: //32
                return 'E_CORE_WARNING';
            case E_CORE_ERROR: //64
                return 'E_COMPILE_ERROR';
            case E_CORE_WARNING: //128
                return 'E_COMPILE_WARNING';
            case E_USER_ERROR: //256
                return 'E_USER_ERROR';
            case E_USER_WARNING: //512
                return 'E_USER_WARNING';
            case E_USER_NOTICE: //1024
                return 'E_USER_NOTICE';
            case E_STRICT: //2048
                return 'E_STRICT';
            case E_RECOVERABLE_ERROR: //4096
                return 'E_RECOVERABLE_ERROR';
            case E_DEPRECATED: //8192
                return 'E_DEPRECATED';
            case E_USER_DEPRECATED: //16384
                return 'E_USER_DEPRECATED';
            case 0:
                return 'UNCAUGHT EXCEPTION';
            default:
                return '';
        }
    }
}
