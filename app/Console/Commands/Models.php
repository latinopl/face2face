<?php

namespace App\Console\Commands;

use Doctrine\DBAL\Platforms\MySQL57Platform;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Driver\PDOMySql\Driver as DoctrineDriver;
use Doctrine\DBAL\Connection as DoctrineConnection;
use Illuminate\Console\DetectsApplicationNamespace;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class Models extends Command
{
    use DetectsApplicationNamespace;
    
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:models';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate models from database.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $conn = \DB::connection();
        
        $driver = new DoctrineDriver();
        $data = ['pdo' => $conn->getPdo(), 'dbname' => config('database.connections.mysql.database'), 'platform' => new MySQL578Platform];
        $doctrine = new DoctrineConnection($data, $driver);
        $dsm = $driver->getSchemaManager($doctrine);
        
        //$dsm = $conn->getDoctrineSchemaManager();
        $prefix = $conn->getTablePrefix();

        $this->loadTables($dsm, $prefix);
    }

    /**
     * @param AbstractSchemaManager $dsm
     * @param string $prefix
     */
    private function loadTables(AbstractSchemaManager $dsm, $prefix)
    {
        $useTimestamps = false;
        $useNamespaces = true;
        $namespaceSize = 1;

        $rootNamespace = trim($this->getAppNamespace(), '\\');
        $baseNamespace = $rootNamespace.'\Models';
        $baseDirectory = app_path('Models');
        
        $tables = $dsm->listTables();
        
        foreach ($tables as $table)
        {
            $tableName = $this->getTableName($table, $prefix);
            
            // Nie generujemy modelu dla tabeli z migracjami
            if ($tableName == config('database.migrations')) continue;
            if ($tableName == config('cache.stores.database-ext.table')) continue;
            
            $classDirectory = $baseDirectory;
            $classNamespace = $baseNamespace;
            $className = '';
            
            if ($useNamespaces == true)
            {
                $namespaceParts = explode('_', $tableName);
                $namespaceParts = array_map('ucfirst', $namespaceParts);
                $namespaceParts = [implode('', $namespaceParts)];
                $offset = min($namespaceSize, count($namespaceParts) - 1);
                $nameParts = array_splice($namespaceParts, $offset);
                $classDirectory = implode(DIRECTORY_SEPARATOR, array_merge([$baseDirectory], $namespaceParts));
                $classNamespace = implode('\\', array_merge([$baseNamespace], $namespaceParts));
                
                $className = implode('', $nameParts);
                $className = Str::singular($className);
            }
            else
            {
                $className = Str::studly($tableName);
                $className = Str::singular($className);
            }
            $this->createBaseClass($classDirectory, $classNamespace, $className, $useTimestamps, $tableName, $table);
            $this->createModelClass($classDirectory, $classNamespace, $className);
        }
    }

    /**
     * @param Table $table
     * @param string $prefix
     * @return string
     */
    private function getTableName(Table $table, $prefix)
    {
        $tableName = $table->getName();

        if (Str::startsWith($tableName, $prefix))
        {
            $tableName = substr($tableName, strlen($prefix));
        }
        
        return $tableName;
    }

    /**
     * @param string $classDirectory
     * @param string $classNamespace
     * @param string $className
     * @param bool $useTimestamps
     * @param string $tableName
     * @param Table $table
     */
    private function createBaseClass($classDirectory, $classNamespace, $className, $useTimestamps, $tableName, Table $table)
    {
        $classDirectory = $classDirectory.DIRECTORY_SEPARATOR.'Base';
        $classNamespaceBase = $classNamespace.'\\Base';
        $classNameBase = $className.'Abstract';
        
        if (\File::exists($classDirectory.DIRECTORY_SEPARATOR.$classNameBase.'.php')) return;
        
        $useTimestamps = $useTimestamps ? 'true' : 'false';
        
        $columns = from($table->getColumns());
        
        $properties = $columns
            ->toDictionary(
                function($v, $k) { return $v->getName(); },
                function($v, $k) { return $this->getType($v->getType()); }
            );
        
        $dates = $columns
            ->where(function($v)
            {
                return $this->getType($v->getType()) == '\Carbon\Carbon';
            })
            ->select(function($v)
            {
                return $v->getName();
            })
            ->toArray();

        $nullable = $columns
            ->where(function(\Doctrine\DBAL\Schema\Column $v)
            {
                return !$v->getNotnull();
            })
            ->select(function($v)
            {
                return $v->getName();
            })
            ->toArray();
        
        $code = "<?php namespace {$classNamespaceBase};

use Illuminate\\Database\\Eloquent\\Model;
use App\\Extensions\\Eloquent\\Traits\\FixedFields;
use App\\Extensions\\Eloquent\\Traits\\NullableFields;

/**
 * Class {$classNameBase}
 *
 * @package {$classNamespaceBase}
 *
 * @mixin \\Illuminate\\Database\\Query\\Builder
 * @mixin \\Illuminate\\Database\\Eloquent\\Builder
 * @method static \\{$classNamespace}\\{$className} query()
 * @method static \\{$classNamespace}\\{$className} find(\$id)
 * @method static \\{$classNamespace}\\{$className} onlyTrashed()
 * @method static \\{$classNamespace}\\{$className} withTrashed()
 * @method static \\{$classNamespace}\\{$className} with(array \$relations)
 * @method \\Illuminate\\Support\\Collection pluck(\$column, \$key = null)
 *
{$this->renderAutocomplete($properties)}
 */
abstract class {$classNameBase} extends Model
{
    use FixedFields, NullableFields;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected \$table = '{$tableName}';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public \$timestamps = {$useTimestamps};

    /**
     * The database column names used by the model.
     *
     * @var array
     */
    protected \$properties = [
{$this->renderProperties($properties)}
    ];

    /**
     * The database column names used by the model.
     *
     * @var array
     */
    protected \$fillable = [
        
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected \$dates = [
{$this->renderDates($dates)}
    ];
    
    /**
     * The attributes that should be saved as null if value is empty.
     *
     * @var array
     */
    protected \$nullable  = [
{$this->renderNullable($nullable)}
    ];
    
    /**
     * Returns all fields from model
     * 
     * @return array
     */
    public function getFields()
    {
        return \$this->properties;
    }
}
";
        
        \File::makeDirectory($classDirectory, 0777, true, true);
        \File::put($classDirectory.DIRECTORY_SEPARATOR.$classNameBase.'.php', $code);
    }

    /**
     * @param string $classDirectory
     * @param string $classNamespace
     * @param string $className
     */
    private function createModelClass($classDirectory, $classNamespace, $className)
    {
        $filePath = $classDirectory.DIRECTORY_SEPARATOR.$className.'.php';
        
        // Nie generujemy pliku jeśli istnieje
        if (\File::exists($filePath)) return;

        $code = "<?php namespace {$classNamespace};

use {$classNamespace}\\Base\\{$className}Abstract;

/**
 * Class {$className}
 *
 * @package {$classNamespace}
 */
class {$className} extends {$className}Abstract
{
}
";
        
        \File::makeDirectory($classDirectory, 0777, true, true);
        \File::put($classDirectory.DIRECTORY_SEPARATOR.$className.'.php', $code);

        $this->info("Created model class: $classNamespace\\$className");
    }

    /**
     * @param Type $typeObject
     * @return string
     */
    private function getType(Type $typeObject)
    {
        $type = '';
        
        switch ($typeObject->getName())
        {
            case 'string':
            case 'text':
            case 'guid':
                $type = 'string';
                break;
            case 'date':
            case 'time':
            case 'datetime':
            case 'datetimetz':
                $type = '\Carbon\Carbon';
                break;
            case 'smallint':
            case 'integer':
            case 'bigint':
                $type = 'integer';
                break;
            case 'decimal':
            case 'float':
                $type = 'float';
                break;
            case 'boolean':
                $type = 'boolean';
                break;
            default:
                $type = 'mixed';
                break;
        }
        
        return $type;
    }

    /**
     * @param array $methods
     * @return string
     */
    private function renderMethods(array $methods)
    {
        $items = [];

        foreach ($methods as $method)
        {
            $items[] = " * @method $method";
        }

        return implode(PHP_EOL, $items);
    }
    
    /**
     * @param array $properties
     * @return string
     */
    private function renderAutocomplete(array $properties)
    {
        $items = [];
        
        foreach ($properties as $name => $type)
        {
            $items[] = " * @property $type $name";
        }
        
        return implode(PHP_EOL, $items);
    }

    /**
     * @param array $properties
     * @return string
     */
    private function renderProperties(array $properties)
    {
        $items = [];

        foreach ($properties as $name => $type)
        {
            $items[] = "        '$name'";
        }

        return implode(",".PHP_EOL, $items);
    }
    
    

    /**
     * @param array $dates
     * @return string
     */
    private function renderDates(array $dates)
    {
        $items = [];

        foreach ($dates as $name)
        {
            $items[] = "        '$name'";
        }

        return implode(",".PHP_EOL, $items);
    }

    /**
     * @param array $nullable
     * @return string
     */
    private function renderNullable(array $nullable)
    {
        $items = [];

        foreach ($nullable as $name)
        {
            $items[] = "        '$name'";
        }

        return implode(",".PHP_EOL, $items);
    }
}

/**
 * Class MySQL578Platform
 * 
 * Tymczasowo, dopóki w Doctrine nie zaimplementują obsługi typu JSON w MySQLu.
 * https://github.com/doctrine/dbal/pull/2266 
 *
 * @package Edito\Console\Commands
 */
class MySQL578Platform extends MySQL57Platform
{
    /**
     * {@inheritdoc}
     */
    protected function initializeDoctrineTypeMappings()
    {
        parent::initializeDoctrineTypeMappings();
        
        $this->doctrineTypeMapping['json'] = 'json_array';
    }

    /**
     * {@inheritdoc}
     */
    public function hasNativeJsonType()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getJsonTypeDeclarationSQL(array $field)
    {
        return 'JSON';
    }
}
