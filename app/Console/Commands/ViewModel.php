<?php

namespace App\Console\Commands;

use Doctrine\DBAL\Platforms\MySQL57Platform;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Driver\PDOMySql\Driver as DoctrineDriver;
use Doctrine\DBAL\Connection as DoctrineConnection;
use Illuminate\Console\DetectsApplicationNamespace;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class ViewModel extends Command
{
    use DetectsApplicationNamespace;
    
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'make:viewmodel {model} {prefix}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate viewmodel for model.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $modelName = $this->argument('model');
        $baseDirectory = "app/Http/ViewModels/{$modelName}";
        $prefix = $this->argument('prefix');
        $postFix = 'ViewModel';
        $className = ucfirst($prefix) . ucfirst($postFix);
        $fileName = $className . '.php';
        $path = $baseDirectory . $fileName;
        $namespace = ucfirst(str_replace('/', '\\', $baseDirectory));
        
        $this->createViewModel($baseDirectory, $namespace, $className, $modelName, $path);
        echo 'View model has been succesfully created';
    }
    
    
    private function createViewModel($baseDirectory, $namespace, $className, $modelName, $path)
    {
        $modelClassPath = 'App\\Models\\' . $modelName;        
        $model = new $modelClassPath;
        $properties = $model -> getFields();
        $code = "<?php namespace {$namespace};

use App\Extensions\ViewModels\ViewModel;
use App\Models\\{$modelName};

/**
 * Class {$className}
 *
 * @package {$namespace}
 */
class {$className} extends ViewModel
{
{$this->renderFields($properties)}
    
    /**
     * @param Content|array \$data
     */
    public function __construct(\$data)
    {
        \$this->fill(\$data);
    }

    /**
     * @return array
     */
    public function getValidatorRules()
    {
        return [
{$this->renderValidationRules($properties)}
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return parent::toArray();
    }
}";
        
        \File::makeDirectory($baseDirectory.'/', 0777, true, true);
        \File::put($path, $code);
    }
    
    /**
     * @param array $properties
     * @return string
     */
    private function renderFields(array $properties)
    {
        $items = [];
        
        foreach ($properties as $name)
        {
            $items[] = "    public \${$name};";
        }
        
        return implode(PHP_EOL, $items);
    }
    
    /**
     * @param array $properties
     * @return string
     */
    private function renderValidationRules(array $properties)
    {
        $items = [];
        
        foreach ($properties as $name)
        {
            $items[] = "            '{$name}' => '',";
        }
        
        return implode(PHP_EOL, $items);
    }
}