<?php namespace App\Console\Commands;

use Gettext\Extractors;
use Gettext\Translation;
use Gettext\Translations;
use Gettext\Translator;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class Translate extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:translate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Extract translations from application.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->fire();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $domains = ['default' => ['name' => 'default', 'locales' => 'resources/lang/pl']];
        $locales = ['pl'];
        
        foreach ($domains as $domain)
        {
            $name = $domain['name'];
            $folder = $domain['locales'];
            
            $this->info("\nDomain: $name ($folder)");
            
            $translations = $this->extractTranslation($domain);

            foreach ($locales as $locale => $localeName)
            {
                $poFile = join(DIRECTORY_SEPARATOR, [$folder, $name.'-'.$locale.'.po']);
                $moFile = join(DIRECTORY_SEPARATOR, [$folder, $name.'-'.$locale.'.mo']);
                
                $summary = "  Locale: $locale";
                
                if (\File::exists($poFile))
                {
                    /** @var Translations $po */
                    $po = Translations::fromPoFile($poFile);

                    $po->setLanguage($locale);
                    $po->setDomain($name);
                    $po->mergeWith($translations);

                    $this->updateReferences($po, $translations);
                    
                    $po->toPoFile($poFile);
                    $summary .= '; '.basename($poFile).' (merged)';
                    
                    $po->toMoFile($moFile);
                    $summary .= '; '.basename($moFile).' (merged)';
                }
                else
                {
                    \File::makeDirectory($folder, 493, true, true);
                    
                    $translations->setLanguage($locale);
                    $translations->setDomain($name);
                    
                    $translations->toPoFile($poFile);
                    $summary .= '; '.basename($poFile).' (created)';

                    $translations->toMoFile($moFile);
                    $summary .= '; '.basename($moFile).' (created)';
                }
                
                $this->info($summary);
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

    /**
     * @param Translations $source
     * @param Translations $translations
     */
    private function updateReferences($source, $translations)
    {
        foreach ($source as $entry)
        {
            /** @var Translation $entry */
            $entry->deleteReferences();
            $entry->deleteExtractedComments();
            
            if (($existing = $translations->find($entry)))
            {
                /** @var Translation $existing */
                foreach ($existing->getReferences() as $reference)
                {
                    $entry->addReference($reference[0], $reference[1]);
                }
                foreach ($existing->getExtractedComments() as $comment)
                {
                    $entry->addExtractedComment($comment);
                }
            }
            else
            {
                $entry->addExtractedComment('Translation has been removed.');
            }
        }
    }

    /**
     * @param array $domain
     * @return Translations
     */
    private function extractTranslation(array $domain)
    {
        $functions = [
            '__' => '__',
            '_' => '__'
        ];
        
        Extractors\PhpCode::$functions = $functions;
        Extractors\JsCode::$functions = $functions;
        
        $entries = new Translations;
        
        foreach ($this->getPaths($domain) as $path)
        {
            if (strstr($path, '.blade.php'))
            {
                $entries->mergeWith(Extractors\Blade::fromFile($path));
            }
            else if (strstr($path, '.php'))
            {
                $entries->mergeWith(Extractors\PhpCode::fromFile($path));
            }
            else if (strstr($path, '.js'))
            {
                $entries->mergeWith(Extractors\JsCode::fromFile($path));
            }
        }
        
        return $entries;
    }

    /**
     * @param $path
     * @return array
     */
    private function scanDirectories($path)
    {
        $files = [];
        
        if (\File::exists($path))
        {
            $directory = new \RecursiveDirectoryIterator($path, \RecursiveDirectoryIterator::SKIP_DOTS);
            $iterator = new \RecursiveIteratorIterator($directory, \RecursiveIteratorIterator::LEAVES_ONLY);
            
            foreach ($iterator as $fileinfo)
            {
                $name = $fileinfo->getPathname();

                if (!strpos($name, '/.'))
                {
                    $files[] = $name;
                }
            }
        }
        
        return $files;
    }
    
    /**
     * @param array $domain
     * @return array
     */
    private function getPaths($domain)
    {
        $paths = [];
        $extension = isset($domain['ext']) ? $domain['ext'] : '.php';
        $sources   = isset($domain['sources']) ? $domain['sources'] : [];
        $exclude   = isset($domain['exclude']) ? $domain['exclude'] : [];
        $sources[] = 'resources';
        // Sources
        foreach ($sources as $path)
        {
            if (Str::endsWith($path, '*'))
            {
                $folders = glob($path, GLOB_ONLYDIR | GLOB_NOSORT);

                foreach (new \DirectoryIterator(rtrim($path, '*')) as $info)
                {
                    if ($info->isFile())
                    {
                        $paths[] = $info->getPathname();
                    }
                }

                foreach ($folders as $folder)
                {
                    $dir = new \RecursiveDirectoryIterator($folder);
                    
                    foreach (new \RecursiveIteratorIterator($dir) as $info)
                    {
                        if ($info->isFile())
                        {
                            $paths[] = $info->getPathname();
                        }
                    }
                }
            }
            else
            {
                $paths = array_merge($paths, glob($path));
            }
        }
        
        // Exclude
        return array_filter($paths, function($path) use ($extension, $exclude)
        {
            return Str::startsWith($path, $exclude) == false && Str::endsWith($path, $extension);
        });
    }
}
