<?php namespace App\Models;

use App\Models\Base\PostcommentAbstract;

/**
 * Class Postcomment
 *
 * @package App\Models
 */
class Postcomment extends PostcommentAbstract
{
    public static function getCommentsForPost($postId, $limit = 10, $offset = 0)
    {
        $query = self::query();
        
        $query->where('post_id', $postId);
        $query->orderBy('created_at', 'DESC');
        $query->limit($limit);
        $query->offset($offset);
        
        $comments = $query->get();
        
        $results = [];
        foreach($comments as $comment)
        {
            if($comment->root_comment_id > 0)
            {
                $results[$comment->root_comment_id][$comment->id] = [
                    'id' => $comment->id,
                    'message' => $comment->message,
                    'time' => $comment->created_at,
                    'user' => User::where('id', $comment->user_id)
                ];
            }
            else
            {
                $results[$comment->id] = [
                    'id' => $comment->id,
                    'message' => $comment->message,
                    'time' => $comment->created_at,
                    'user' => User::where('id', $comment->user_id),
                    'childs' => []
                ];
            }
        }
        return $results;
    }
    
    public static function getNumberOfComments($postId)
    {
        return self::query()->where('post_id', $postId)->count();
    }
}
