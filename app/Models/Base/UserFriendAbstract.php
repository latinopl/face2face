<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;
use App\Extensions\Eloquent\Traits\FixedFields;
use App\Extensions\Eloquent\Traits\NullableFields;

/**
 * Class FriendAbstract
 *
 * @package App\Models\Base
 *
 * @mixin \Illuminate\Database\Query\Builder
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @method static \App\Models\UserFriend query()
 * @method static \App\Models\UserFriend find($id)
 * @method static \App\Models\UserFriend onlyTrashed()
 * @method static \App\Models\UserFriend withTrashed()
 * @method static \App\Models\UserFriend with(array $relations)
 * @method \Illuminate\Support\Collection pluck($column, $key = null)
 *
 * @property integer user_id
 * @property integer friend_id
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 * @property boolean updated_at
 */
abstract class UserFriendAbstract extends Model
{
    use FixedFields, NullableFields;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_friends';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The database column names used by the model.
     *
     * @var array
     */
    protected $properties = [
        'user_id',
        'friend_id',
        'created_at',
        'updated_at',
        'accepted'
    ];

    /**
     * The database column names used by the model.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'friend_id',
        'created_at',
        'updated_at',
        'accepted'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];
    
    /**
     * The attributes that should be saved as null if value is empty.
     *
     * @var array
     */
    protected $nullable  = [
        'created_at',
        'updated_at'
    ];
    
    /**
     * Returns all fields from model
     * 
     * @return array
     */
    public function getFields()
    {
        return $this->properties;
    }
}
