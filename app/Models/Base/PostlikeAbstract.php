<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;
use App\Extensions\Eloquent\Traits\FixedFields;
use App\Extensions\Eloquent\Traits\NullableFields;

/**
 * Class PostlikeAbstract
 *
 * @package App\Models\Base
 *
 * @mixin \Illuminate\Database\Query\Builder
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @method static \App\Models\Postlike query()
 * @method static \App\Models\Postlike find($id)
 * @method static \App\Models\Postlike onlyTrashed()
 * @method static \App\Models\Postlike withTrashed()
 * @method static \App\Models\Postlike with(array $relations)
 * @method \Illuminate\Support\Collection pluck($column, $key = null)
 *
 * @property integer user_id
 * @property integer post_id
 */
abstract class PostlikeAbstract extends Model
{
    use FixedFields, NullableFields;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'postlikes';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The database column names used by the model.
     *
     * @var array
     */
    protected $properties = [
        'user_id',
        'post_id'
    ];

    /**
     * The database column names used by the model.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'post_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [

    ];
    
    /**
     * The attributes that should be saved as null if value is empty.
     *
     * @var array
     */
    protected $nullable  = [

    ];
    
    /**
     * Returns all fields from model
     * 
     * @return array
     */
    public function getFields()
    {
        return $this->properties;
    }
}
