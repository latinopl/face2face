<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
/**
 * Class UserAbstract
 *
 * @package App\Models\Base
 *
 * @mixin \Illuminate\Database\Query\Builder
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @method static \App\Models\User query()
 * @method static \App\Models\User find($id)
 * @method static \App\Models\User onlyTrashed()
 * @method static \App\Models\User withTrashed()
 * @method static \App\Models\User with(array $relations)
 * @method \Illuminate\Support\Collection pluck($column, $key = null)
 *
 * @property integer id
 * @property string email
 * @property string password
 * @property string userlogin
 * @property string first_name
 * @property string second_name
 * @property string nick_name
 * @property string activation_code
 * @property string password_history
 * @property string is_profile_public
 * @property string is_activated
 * @property string is_logged
 * @property \Carbon\Carbon last_signin
 * @property \Carbon\Carbon signup_date
 * @property \Carbon\Carbon last_password_change
 * @property \Carbon\Carbon last_account_change
 * @property string about_me
 * @property \Carbon\Carbon date_of_birth
 * @property string sex
 * @property string main_photo
 * @property \Carbon\Carbon last_logout
 */
abstract class UserAbstract extends Authenticatable
{
    use Notifiable;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The database column names used by the model.
     *
     * @var array
     */
    protected $properties = [
        'id',
        'email',
        'password',
        'userlogin',
        'first_name',
        'second_name',
        'nick_name',
        'activation_code',
        'password_history',
        'is_profile_public',
        'is_activated',
        'is_logged',
        'last_signin',
        'signup_date',
        'last_password_change',
        'last_account_change',
        'about_me',
        'date_of_birth',
        'sex',
        'main_photo',
        'last_logout'
    ];
    
    /**
     * The database column names used by the model.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'userlogin',
        'first_name',
        'second_name',
        'nick_name',
        'date_of_birth',
        'sex'
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'last_signin',
        'signup_date',
        'last_password_change',
        'last_account_change',
        'date_of_birth',
        'last_logout'
    ];
    
    /**
     * The attributes that should be saved as null if value is empty.
     *
     * @var array
     */
    protected $nullable  = [

    ];
    
    /**
     * Returns all fields from model
     * 
     * @return array
     */
    public function getFields()
    {
        return $this->properties;
    }
}