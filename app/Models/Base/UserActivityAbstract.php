<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;
use App\Extensions\Eloquent\Traits\FixedFields;
use App\Extensions\Eloquent\Traits\NullableFields;

/**
 * Class UserActivityAbstract
 *
 * @package App\Models\Base
 *
 * @mixin \Illuminate\Database\Query\Builder
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @method static \App\Models\UserActivity query()
 * @method static \App\Models\UserActivity find($id)
 * @method static \App\Models\UserActivity onlyTrashed()
 * @method static \App\Models\UserActivity withTrashed()
 * @method static \App\Models\UserActivity with(array $relations)
 * @method \Illuminate\Support\Collection pluck($column, $key = null)
 *
 * @property integer id
 * @property integer user_id
 * @property string action
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 */
abstract class UserActivityAbstract extends Model
{
    use FixedFields, NullableFields;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_activity';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The database column names used by the model.
     *
     * @var array
     */
    protected $properties = [
        'id',
        'user_id',
        'action',
        'created_at',
        'updated_at'
    ];

    /**
     * The database column names used by the model.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];
    
    /**
     * The attributes that should be saved as null if value is empty.
     *
     * @var array
     */
    protected $nullable  = [
        'created_at',
        'updated_at'
    ];
    
    /**
     * Returns all fields from model
     * 
     * @return array
     */
    public function getFields()
    {
        return $this->properties;
    }
}
