<?php namespace App\Models;

use App\Models\Base\UserFriendAbstract;
use Illuminate\Support\Facades\Auth;

/**
 * Class Friend
 *
 * @package App\Models\User
 */
class UserFriend extends UserFriendAbstract
{
    public static function areWeFriends($friend_id)
    {
        $rel = self::query()->where('friend_id', $friend_id)->where('user_id', Auth::user()->id)->first();
        if(!empty($rel->user_id))
        {
            return true;
        }
        
        return false;
    }
}
