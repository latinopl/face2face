<?php namespace App\Models;

use App\Models\Base\PostAbstract;
use Illuminate\Support\Facades\Auth;
use App\Models\Postcomment;
use Illuminate\Support\Facades\DB;

/**
 * Class Post
 *
 * @package App\Models
 */
class Post extends PostAbstract
{
    public static function getPostsForUser($user_id, $limit, $offset)
    {
        $user = '';
        if($user_id > 0)
        {
            $query = ' p.user_id = '.$user_id . ' AND p.visibility < 2';
        }
        else
        {
            $query = ' p.user_id = '.Auth::user()->id.' OR p.visibility = 0 OR (p.user_id IN (SELECT friend_id FROM user_friends WHERE user_id = '.Auth::user()->id.') AND p.visibility < 2) ';
        }
        $posts = DB::select('select p.*, count(pl.post_id) as likes FROM posts as p LEFT JOIN postlikes as pl ON pl.post_id = p.id WHERE '.$query.' GROUP By p.id ORDER BY p.created_at DESC LIMIT :limit OFFSET :offset', ['limit' => $limit, 'offset' => $offset]);
        
        $result = [];
        foreach($posts as $post)
        {
            $icon = 'f2f-';
            switch((int)$post->visibility)
            {
                case 0:
                    $icon .= 'globe';
                    break;
                case 1:
                    $icon .= 'group';
                    break;
                case 2:
                    $icon .= 'lock';
                    break;
                default:
                    $icon .= 'globe';
                    break;
            }
            $comments = Postcomment::getNumberOfComments($post->id);
            
            $user = User::where('id', $post->user_id)->first();
            $userLike = Postlike::query()->where('user_id', Auth::user()->id)->where('post_id', $post->id)->first();
            $likeIcon = 'f2f-thumbs-o-up';
            if(!empty($userLike->post_id))
            {
                $likeIcon = 'f2f-thumbs-up';
            }
            $result[] = [
                'id' => $post->id,
                'message' => $post->message,
                'likes' => $post->likes,
                'ammountOfComments' => $comments,
                'comments' => [],
                'time' => date($post->created_at),
                'visibility' => [
                    'visibility' => $post->visibility,
                    'icon' => $icon
                ],
                'displayCommentsSection' => false,
                'user' => [
                    'profileLink' => route('profile', ['id' => $user->id]),
                    'name' => $user->first_name . ' ' . $user->second_name,
                    'image' => User::getMainPhotoUrl($user)
                ],
                'likeIcon' => $likeIcon,
                'commentsLimit' => 10,
                'commentsOffset' => 0,
                'commentModel' => [
                    'message'
                ],
                'showMoreComments' => false
            ];
        }
        
        return $result;
    }
}
